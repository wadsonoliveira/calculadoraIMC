# CalculadoraImc
Este projeto visa demonstrar alguns conceitos e técnicas de DevOps como controle de versão usando GitLab, criação de container usando Docker e orquestração de container usando kubernetes.

A aplicação se trata de uma single page usando Angular para calcular o IMC.

O passo-a-passo é:
1- criação de uma single page usando angular;
2- Criada a branch develop e bloqueada a branch main apenas para maintainers;
3- Criada TAG de versão das releases;
4- Criado Dockerfile e Docker ignore;
5- Criada a imagem Docker e criado o container com mapeamento de porta (4200 do container);
6- Não houve comunicação entre containers;
7- Não foi criado volume para salvar dados do container;
8- Não foi utilizado Kubernetes para orquestrar o container.